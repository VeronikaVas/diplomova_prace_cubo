/**
 * @license
 * Copyright 2018 Google LLC
 * SPDX-License-Identifier: Apache-2.0
 */

/**
 * @fileoverview Classic theme.
 * Contains multi-coloured border to create shadow effect.
 */
'use strict';

goog.provide('Blockly.Themes.Classic');

goog.require('Blockly.Theme');


// Temporary holding object.
Blockly.Themes.Classic = {};

Blockly.Themes.Classic.defaultBlockStyles = {
  "colour_blocks": {
    "colourPrimary": "#BFAE48"
  },
  "list_blocks": {
    "colourPrimary": "#3F51B5"
  },
  "logic_blocks": {
    "colourPrimary": "#673AB7"
  },
  "loop_blocks": {
    "colourPrimary": "#9C27B0"
  },
  "math_blocks": {
    "colourPrimary": "#E91E63"
  },
  "procedure_blocks": {
    "colourPrimary": "#00BCD4"
  },
  "text_blocks": {
    "colourPrimary": "#F44336"
  },
  "variable_blocks": {
    "colourPrimary": "#2196F3"
  },
  "variable_dynamic_blocks": {
    "colourPrimary": "#2196F3"
  },
  "canvas_blocks": {
    "colourPrimary": "#009688"
  },
  "lines_blocks": {
    "colourPrimary": "#4CAF50"
  },
  "shapes_blocks": {
    "colourPrimary": "#FF9800"
  },
  "draw_text_blocks": {
    "colourPrimary": "#FF9800"
  },

  "hat_blocks": {
    "colourPrimary": "330",
    "hat": "cap"
  }
};

Blockly.Themes.Classic.categoryStyles = {
  "colour_category": {
    "colour": "20"
  },
  "list_category": {
    "colour": "#6369D1"
  },
  "logic_category": {
    "colour": "#7B4B94"
  },
  "loop_category": {
    "colour": "#D3307E"
  },
  "math_category": {
    "colour": "#A4031F"
  },
  "procedure_category": {
    "colour": "#044B7F"
  },
  "text_category": {
    "colour": "#E24E1B"
  },
  "variable_category": {
    "colour": "#016FB9"
  },
  "variable_dynamic_category": {
    "colour": "#016FB9"
  }
  ,
  "canvas_category": {
    "colour": "#349029"
  },
  "lines_category": {
    "colour": "#097054"
  },
  "shapes_category": {
    "colour": "#82735C"
  },
  "draw_text_category": {
    "colour": "#795548"
  }
};

Blockly.Themes.Classic =
  new Blockly.Theme('classic', Blockly.Themes.Classic.defaultBlockStyles,
    Blockly.Themes.Classic.categoryStyles);
