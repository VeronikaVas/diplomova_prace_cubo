Generování knihovny
----------------------------------------------------------------
Při změně v knihovně je nutné přegenerovat pomocí
python build.py

Po přegenerování uděláme export souborů pro použití jako balíčku
npm run package

a následně ve složce dist/
npm pack

což vytvoří lokální balíček.

Provedené změny
----------------------------------------------------------------
generators/javascript/loops - změna podmínky for cyklu
core/theme/ - úprava barev
core/renderers - změna konstanty pro zaoblení rohu bloku
core/block_svg - povolení deprecated
core/css - úprava css pro změnu vzhledu toolboxu, změna vzhledu tooltipu
core/scrollbar - úprava tloušťky scrollbaru --> problém v mozzile/IE/edge
core/toolbox - úprava generování toolboxu, aby šla vložit ikona kategorie
core/variables - vytvořeno generování nového bloku pro lokální proměnnou
msg/js/cs - úprava překladu


