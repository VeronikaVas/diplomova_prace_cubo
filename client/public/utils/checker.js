self.importScripts('./acorn_interpreter.js')

onmessage = (e) => {
  var userSolution = e.data.userSolution
  var solution = e.data.solution
  switch (e.data.type) {
    case 'MATCH':
      console.log(e.data.userSolution == e.data.solution)
      console.log(e.data.solution)
      postMessage(
        e.data.userSolution == e.data.solution
      )
      break

    case 'DRAWING':
      postMessage(checkImages(userSolution, solution))
      break

    case 'FUNCTION':
      checkFunctions(userSolution, solution)
      break
  }
}

function checkImages(userImage, answerImage) {
  var len = Math.min(userImage.data.length, answerImage.data.length)
  var delta = 0
  // pixely jsou RGB, staci kontrolovat aplha bytes
  for (var i = 3; i < len; i += 4) {
    // kontrola
    if (Math.abs(userImage.data[i] - answerImage.data[i]) > 64) {
      delta++
    }
  }

  return isCorrect(delta)
}

function isCorrect(pixelErrors) {
  if (pixelErrors > 100) {
    // Too many errors.
    return false
  }
  return true
}

function parseFunctionName(text) {
  var i = text.match(/([a-zA-Z_{1}][a-zA-Z0-9_]+)(?=\()/g)
  if (i.length > 0) {
    return i[0]
  }
}

String.prototype.replaceAll = function (search, replacement) {
  var target = this
  return target.replace(new RegExp(search, 'g'), replacement)
}

function parseFunctionCalls(text) {
  return text.substring(text.lastIndexOf('}') + 1, text.length)
}

function initApi(interpreter, scope) {
  interpreter.setProperty(
    scope,
    'alert',
    interpreter.createNativeFunction(val => {
      val = val ? val.toString() : ''
    })
  )
}

function checkFunctions(userSolutionCode, solutionCode) {
  var solutionFceName = parseFunctionName(solutionCode)
  console.log(solutionCode)
  var userCheckCalls = parseFunctionCalls(solutionCode).trim()
  userCheckCalls = userCheckCalls.replaceAll('\n', ' ').replace(/ {1,}/g, ' ').split(';')
  userCheckCalls.splice(-1, 1)

  solutionCode = solutionCode.replaceAll(solutionFceName, solutionFceName + '1') // prejmenuj nazvy a volani puvodni fce
  var solutionCalls = parseFunctionCalls(solutionCode).trim()
  solutionCalls = solutionCalls.replaceAll('\n', ' ').replace(/ {1,}/g, ' ').split(';')
  solutionCalls.splice(-1, 1)
  console.log(userCheckCalls)
  console.log(solutionCalls)

  var fce = solutionCode.substring(0, solutionCode.lastIndexOf('}') + 1).replaceAll('\n', ' ')
  console.log(fce)

  var userFce = userSolutionCode.substring(0, userSolutionCode.lastIndexOf('}') + 1).replaceAll('\n', ' ')
  console.log(userFce)

  var s = 'function assert(f1, f2){ \n\n return f1 === f2; \n} \n'
  var interpreter = new self.Interpreter(s, initApi)
  interpreter.appendCode(fce)
  interpreter.appendCode(userFce)
  interpreter.appendCode('var ooo =' + JSON.stringify(solutionCalls) + '; var sss = ' + JSON.stringify(userCheckCalls) + '; var ttt12 = true;')

  interpreter.appendCode('for(var iii=0; iii < ooo.length; iii++){ if(!assert(eval(ooo[iii]), eval(sss[iii]))){ ttt12 = false; break;} } ttt12;')

  interpreter.run()

  postMessage(interpreter.value.data)
}
