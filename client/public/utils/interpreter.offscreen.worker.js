self.importScripts('./acorn_interpreter.js')
self.importScripts('./drawing-api.js')

onmessage = e => {
  this.Canvas.initContext()
  var interpreter = new this.Interpreter(e.data.code, self.initApi)
  try {
    interpreter.run()
    var bitmapOne = this.Canvas.getCanvas().transferToImageBitmap()

    postMessage({ output: null, canvas: bitmapOne, color: this.Canvas.color.data })
    // postMessage({ output: null, canvas: null })
    postMessage({ output: "---Konec programu---", canvas: null })
    close()
  } catch (error) {
    postMessage({ error: error })
    close()
  }
}

self.initApi = function (interpreter, scope) {
  interpreter.setProperty(
    scope,
    'alert',
    interpreter.createNativeFunction(val => {
      val = val ? val.toString() : ''
      // postMessage(val)
      postMessage({ output: val, canvas: null })
    })
  )
  self.Canvas.initInterpreter(interpreter, scope)
}
