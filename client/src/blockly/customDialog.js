import * as Blockly from 'blockly/core';
var CustomDialog = {};

/**
 * An implementation of Blockly's demo dialogs.
 *
 */

Blockly.alert = function (message, callback) {
    CustomDialog.show('', message, {
        onCancel: callback
    });
};

Blockly.confirm = function (message, callback) {
    CustomDialog.show('', message, {
        showOkay: true,
        onOkay: function () {
            callback(true)
        },
        showCancel: true,
        onCancel: function () {
            callback(false)
        }
    })
};

Blockly.prompt = function (message, defaultValue, callback) {
    CustomDialog.show('', message, {
        showInput: true,
        showOkay: true,
        onOkay: function () {
            callback(CustomDialog.inputField.value)
        },
        showCancel: true,
        onCancel: function () {
            callback(null)
        }
    })
    CustomDialog.inputField.value = defaultValue
}

CustomDialog.hide = function () {
    if (CustomDialog.backdropDiv_) {
        CustomDialog.backdropDiv_.style.display = 'none'
        CustomDialog.dialogDiv_.style.display = 'none'
    }
}

CustomDialog.show = function (title, message, options) {
    var backdropDiv = CustomDialog.backdropDiv_
    var dialogDiv = CustomDialog.dialogDiv_
    if (!dialogDiv) {
        // Generate HTML
        backdropDiv = document.createElement('div')
        backdropDiv.className = 'modal-backdrop'
        backdropDiv.classList.add("fading")
        //backdropDiv.classList.add("modal-fade-leave-active")

        document.body.appendChild(backdropDiv)

        dialogDiv = document.createElement('div')
        dialogDiv.className = 'modal'
        backdropDiv.appendChild(dialogDiv)

        dialogDiv.onclick = function (event) {
            event.stopPropagation()
        }

        CustomDialog.backdropDiv_ = backdropDiv
        CustomDialog.dialogDiv_ = dialogDiv
    }
    backdropDiv.style.display = 'flex'
    backdropDiv.style.visibility = "visible"
    dialogDiv.style.display = 'flex'

    dialogDiv.innerHTML =
        (title ? '<div class="modal-header">' + message + '</div>' : '') +
        '<div class="modal-body">' +
        '<p>' + message + '</p>' +
        (options.showInput ? '<input id="customDialogInput" class="cinput">' : '') +
        '</div>' +
        '<div class="modal-footer">' +
        (options.showCancel ? '<button id="customDialogCancel" class="btn is-outlined ">Zrušit</button>' : '') +
        (options.showOkay ? '<button id="customDialogOkay" class="btn is-primary btn-ok">OK</button>' : '') +
        '</div>'
    /*dialogDiv.getElementsByClassName('c-modal-header')[0]
        .appendChild(document.createTextNode(title))
    dialogDiv.getElementsByClassName('customDialogMessage')[0]
        .appendChild(document.createTextNode(message))*/

    var onOkay = function (event) {
        CustomDialog.hide()
        options.onOkay && options.onOkay()
        event && event.stopPropagation()
    }
    var onCancel = function (event) {
        CustomDialog.hide()
        options.onCancel && options.onCancel()
        event && event.stopPropagation()
    }

    var dialogInput = document.getElementById('customDialogInput')
    CustomDialog.inputField = dialogInput
    if (dialogInput) {
        dialogInput.focus()

        dialogInput.onkeyup = function (event) {
            if (event.keyCode == 13) {
                // Process as OK when user hits enter.
                onOkay()
                return false
            } else if (event.keyCode == 27) {
                // Process as cancel when user hits esc.
                onCancel()
                return false
            }
        }
    } else {
        var okay = document.getElementById('customDialogOkay')
        okay && okay.focus()
    }

    if (options.showOkay) {
        document.getElementById('customDialogOkay')
            .addEventListener('click', onOkay)
    }
    if (options.showCancel) {
        document.getElementById('customDialogCancel')
            .addEventListener('click', onCancel)
    }

    backdropDiv.onclick = onCancel
}