var Debug = {}
Debug.interpreter = null
Debug.lastBlockToHighlight = null
Debug.highlightPause = false
Debug.variables = {}
Debug.variablesLates = {}
Debug.moves = 0
Debug.workspace = null
Debug.api = null

export { Debug }