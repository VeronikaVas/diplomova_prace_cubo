/*eslint-disable */
function genShadow(v, type, name, value) {
    return '<value name="' + v + '"><shadow type="' + type + '"><field name="' + name + '">' + value + '</field></shadow></value>';
}

function genLbl(text) {
    return '<label text="' + text + '"></label>'
}

function genBlock(name) {
    var block = '<block type="' + name + '">';
    for (var i = 1; i < arguments.length; i++) {
        block += arguments[i];
    }
    block += '</block>';
    return block;
}
var toolboxStart = '<xml id="toolbox" style="display: none">';
var toolboxEnd = '</xml>';
var sep = '<sep></sep>';

var textStart = '<category name="Text" style="text_category" id="text-category" icon="icon-text-category" colour="#F44336">';
var textEnd = '</category>';

var mathStart = ' <category name="Matematika" style="math_category" id="math-category" icon="icon-math-category" colour="#E91E63">';
var mathEnd = '</category>';

var loopStart = '<category name="Cykly" style="loop-category" id="loop-category" icon="icon-loop-category" colour="#9C27B0">';
var loopEnd = '</category>';

var logicStart = '<category name="Logika" style="logic_category" id="logic-category" icon="icon-logic-category" colour="#673AB7">';
var logicEnd = '</category>';
var listsStart = '<category name="Seznam" style="list_category" id="list-category" icon="icon-list-ol" colour="#3F51B5">';
var listsEnd = '</category>';

var varsStart = '<category name="Proměnné" style="variable_category" custom="VARIABLE" id="variable-category" icon="icon-variable" colour="#2196F3">'
var varsEnd = '</category>';

var functionStart = '<category name="Funkce" custom="PROCEDURE" style="procedure_category" id="function-category" icon="icon-function" colour="#00BCD4">';
var functionEnd = '</category>';

var canvasStart = '<category name="Plátno" style="canvas_category" id="canvas-category" icon="icon-canvas" colour="#009688">';
var canvasEnd = '</category>';

var linesStart = '<category name="Čára a barvy" style="lines_category" id="lines-category" icon="icon-lines-colors" colour="#4CAF50">'
var linesEnd = '</category>'

var shapesStart = '<category name="Tvary" style="shapes_category" id="shapes-category" icon="icon-shapes" colour="#FF9800">'
var shapesEnd = '</category>'
//var printing = '<block type="text_print"><value name="TEXT"><shadow type="text"><field name="TEXT">abc</field></shadow></value></block>'
var text = textStart
    + genLbl("Vytvoření textu")
    + genBlock("text")
    + genBlock("text_join")
    + genBlock("text_append", genShadow("TEXT", "text", "TEXT", "abc"))
    + genLbl("Práce s textem")
    + genBlock("text_length", genShadow("VALUE", "text", "TEXT", "abc"))
    + genBlock("text_isEmpty", genShadow("VALUE", "text", "TEXT", "abc"))
    + genBlock("cubo_text_charat", genShadow("VALUE", "text", "TEXT", "abc"), genShadow("AT", "math_number", "NUM", 1))
    + genBlock("text_indexOf", genShadow("VALUE", "text", "TEXT", "abc"), genShadow("FIND", "text", "TEXT", "bc"))
    + genBlock("cubo_text_substring", genShadow("VALUE", "text", "TEXT", "abc"), genShadow("FROM", "math_number", "NUM", 1), genShadow("TO", "math_number", "NUM", 1))
    + genBlock("text_trim", genShadow("TEXT", "text", "TEXT", "abc"))
    + genBlock("text_replace", genShadow("FROM", "text", "TEXT", "abc"), genShadow("TO", "text", "TEXT", "ab"), genShadow("TEXT", "text", "TEXT", "aa"))
    + genLbl("Vypsání hodnoty na výstup")
    + genBlock("text_print", genShadow("TEXT", "text", "TEXT", "abc"))
    + textEnd
    + sep;
var math = mathStart
    + genBlock("math_number")
    + genBlock("math_arithmetic", genShadow("A", "math_number", "NUM", 25), genShadow("B", "math_number", "NUM", 5))
    + genBlock("math_modulo", genShadow("DIVIDEND", "math_number", "NUM", 11), genShadow("DIVISOR", "math_number", "NUM", 5))
    + genBlock("math_constant")
    + genBlock("math_trig", genShadow("NUM", "math_number", "NUM", 90))
    + genBlock("math_round", genShadow("NUM", "math_number", "NUM", 90))
    + genBlock("math_random_int", genShadow("FROM", "math_number", "NUM", 1), genShadow("TO", "math_number", "NUM", 1))
    + genBlock("math_random_float")
    + mathEnd + sep;
var loop = loopStart
    + genBlock("controls_whileUntil")
    + genBlock("controls_for", genShadow("FROM", "math_number", "NUM", 1), genShadow("TO", "math_number", "NUM", 10), genShadow("BY", "math_number", "NUM", 1))
    + genBlock("controls_forEach")
    + genBlock("controls_flow_statements")
    + loopEnd
    + sep;
var logic = logicStart
    + genBlock("logic_boolean", genShadow("A", "math_number", "NUM", 25))
    + genBlock("logic_operation")
    + genBlock("logic_negate")
    + genBlock("logic_compare")
    + genBlock("controls_if")
    + logicEnd
    + sep;;

var listVar = '<value name="VALUE"> <block type="variables_get"> <field name="VAR">seznam</field> </block> </value>';
var list = listsStart
    + genLbl("Vytvoření seznamu")
    + genBlock("lists_create_empty")
    + genBlock("lists_create_with", '<mutation items="2"></mutation>')
    + genBlock("lists_repeat", genShadow("NUM", "math_number", "NUM", 5), genShadow("VALUE", "math_number", "NUM", 5))
    + genLbl("Operace se seznamem")
    + genBlock("lists_length", listVar)
    + genBlock("lists_isEmpty", listVar)
    + genBlock("lists_indexOf", listVar, genShadow("FIND", "text", "TEXT", "a"))
    + genBlock("lists_getIndex", listVar)
    + genBlock("lists_setIndex", '<value name="LIST"><block type="variables_get"><field name="VAR">seznam</field></block></value>')
    + genBlock("lists_getSublist", '<value name="LIST"><block type="variables_get"><field name="VAR">seznam</field></block></value>')
    + genBlock("lists_split", genShadow("DELIM", "text", "TEXT", ";"))

    + listsEnd
    + sep;

var vars = varsStart + varsEnd + sep
var functions = functionStart + functionEnd + sep;

var canvas = canvasStart
    + genLbl("Nastavení plátna")
    + genBlock('backgroundcolor', genShadow('fillColour', "colour_picker", "COLOUR", "#ff0000"))
    + genBlock('canvas_set_size', genShadow('value', "math_number", "NUM", 400))
    + genBlock('canvas_get_size')
    + genLbl("Pohyb po plátně")
    + genBlock('moveXY', genShadow('x', "math_number", "NUM", 0), genShadow('y', "math_number", "NUM", 0))
    + genBlock('move_distance', genShadow('distance', "math_number", "NUM", 50))
    + genBlock('move_distance2', genShadow('distance', "math_number", "NUM", 50), genShadow('angle', "math_number", "NUM", 90))
    + genBlock('move_select')
    + genBlock('actual_position')
    + genBlock('actual_position_coordinate')
    + canvasEnd + sep;

var lines = linesStart
    + genLbl("Barva")
    + genBlock("colour_picker")
    + genBlock("colour_random")
    + genBlock("colour_rgb", genShadow("RED", "math_number", "NUM", 100), genShadow("GREEN", "math_number", "NUM", 50), genShadow("BLUE", "math_number", "NUM", 0))
    + genLbl("Nastavení čar a výplně")
    + genBlock("fill", genShadow("fillColour", "colour_picker", "COLOUR", "#000000"))
    + genBlock("stroke", genShadow("stroke_colour", "colour_picker", "COLOUR", "#000000"))
    + genBlock("line_width", genShadow("width", "math_number", "NUM", 2))
    + genBlock("line_cap")
    + genLbl("Kreslení čar")
    + genBlock("line", genShadow("distance", "math_number", "NUM", 50))
    + genBlock("line_angle_input", genShadow("distance", "math_number", "NUM", 50), genShadow("angle", "math_number", "NUM", 90))
    + linesEnd
    + sep;
var shapes = shapesStart
    + genLbl("Tvary")
    + genBlock("draw_rect", genShadow("width", "math_number", "NUM", 50), genShadow("height", "math_number", "NUM", 50))
    + genBlock("draw_circle", genShadow("r", "math_number", "NUM", 50))
    + genBlock("draw_arc", genShadow("r", "math_number", "NUM", 50))
    + genBlock("draw_arc_2", genShadow("r", "math_number", "NUM", 50), genShadow("from", "math_number", "NUM", 180), genShadow("to", "math_number", "NUM", 0))
    + genBlock("path")
    + genLbl("Kreslení textu")
    + genBlock("font", genShadow("size", "math_number", "NUM", 15))
    + genBlock("draw_text", genShadow("text", "text", "TEXT", "abc"))
    + shapesEnd
    + sep;

var toolbox = toolboxStart + text + math + loop + logic + list + vars + functions + canvas + lines + shapes + toolboxEnd
/*eslint-enable */
export { toolbox }