import * as Blockly from 'blockly/core';

Blockly.Msg["BG_COLOR"] = "Nastaví pozadí plátna danou barvou";
Blockly.Msg["CANVAS_SET_SIZE"] = "Nastaví šířku nebo výšku plátna na danou hodnotu";
Blockly.Msg["CANVAS_GET_SIZE"] = "Vrátí aktuální šířku nebo výšku plátna";

Blockly.Msg["CANVAS_MOVEXY"] = "Přesune aktuální pozici do bodu určenému souřadnicemi x a y";
Blockly.Msg["CANVAS_MOVE_DISTANCE"] = "Přesune aktuální pozici do bodu, který je vypočítán pomocí vzdálenosti a úhlu";
Blockly.Msg["CANVAS_MOVE_SELECT"] = "Přesune aktuální pozici do bodu, který je vybrán";
Blockly.Msg["CANVAS_ACTUAL_POS"] = "Zobrazí aktuální pozici na plátno pomocí barevného křížku"
Blockly.Msg["CANVAS_ACTUAL_POS_COORDINATE"] = "Vrátí souřadnici x nebo y aktuální pozice"

Blockly.Msg["CANVAS_HELP_URL"] = "/napoveda#nastaveni-platna";
Blockly.Msg["MOVE_HELP_URL"] = "/napoveda#pohyb-po-platne";

/**pozadi platna */
Blockly.Blocks['backgroundcolor'] = {
    init: function () {
        this.appendValueInput('fillColour')
            .setCheck('Colour')
            .appendField('nastavit pozadí')
        this.setPreviousStatement(true, null)
        this.setNextStatement(true, null)
        this.setColour(230)
        this.setStyle('canvas_blocks')
        this.setTooltip(Blockly.Msg["BG_COLOR"])
        this.setHelpUrl(Blockly.Msg["CANVAS_HELP_URL"])
    }
}

Blockly.JavaScript['backgroundcolor'] = function (block) {
    var fillcolour = Blockly.JavaScript.valueToCode(block, 'fillColour', Blockly.JavaScript.ORDER_ATOMIC)
    return 'setBackgroundColorCubo(' + fillcolour + ');\n'
}

/* setter velikosti platna */
Blockly.Blocks['canvas_set_size'] = {
    init: function () {
        this.appendValueInput("value")
            .setCheck("Number")
            .appendField("nastavit ")
            .appendField(new Blockly.FieldDropdown([["šířku", "WIDTH"], ["výšku", "HEIGHT"]]), "SIZE");
        this.setInputsInline(false);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(230);
        this.setStyle('canvas_blocks')
        this.setTooltip(Blockly.Msg["CANVAS_SET_SIZE"]);
        this.setHelpUrl(Blockly.Msg["CANVAS_HELP_URL"]);
    }
};

Blockly.JavaScript['canvas_set_size'] = function (block) {
    var size = block.getFieldValue('SIZE');
    var value = Blockly.JavaScript.valueToCode(block, 'value', Blockly.JavaScript.ORDER_ATOMIC);
    return 'setSizeCubo(' + value + ',"' + size + '");\n'

};

/* getter velikosti platna */
Blockly.Blocks['canvas_get_size'] = {
    init: function () {
        this.appendDummyInput()
            .appendField(new Blockly.FieldDropdown([["šířka", "WIDTH"], ["výška", "HEIGHT"]]), "SIZE")
            .appendField("plátna");
        this.setOutput(true, "Number");
        this.setStyle('canvas_blocks')
        this.setTooltip(Blockly.Msg["CANVAS_GET_SIZE"]);
        this.setHelpUrl(Blockly.Msg["CANVAS_HELP_URL"]);
    }
}

Blockly.JavaScript['canvas_get_size'] = function (block) {
    var size = block.getFieldValue('SIZE');
    return ['getSizeCubo("' + size + '")', Blockly.JavaScript.ORDER_NONE];
};

/** posunuti se na pozici o souradnidnich x a y */
Blockly.Blocks['moveXY'] = {
    init: function () {
        this.appendDummyInput()
            .appendField('posun na')
        this.appendValueInput('x')
            .setCheck('Number')
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField('x')
        this.appendValueInput('y')
            .setCheck('Number')
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField('y')
        this.setPreviousStatement(true, null)
        this.setNextStatement(true, null)
        this.setInputsInline(true)
        this.setStyle('canvas_blocks')
        this.setTooltip(Blockly.Msg["CANVAS_MOVEXY"])
        this.setHelpUrl(Blockly.Msg["MOVE_HELP_URL"])
    }
}

Blockly.JavaScript['moveXY'] = function (block) {
    var x = Blockly.JavaScript.valueToCode(block, 'x', Blockly.JavaScript.ORDER_ATOMIC)
    var y = Blockly.JavaScript.valueToCode(block, 'y', Blockly.JavaScript.ORDER_ATOMIC)
    return 'moveXYCubo(' + x + ',' + y + ');\n'
}

Blockly.Blocks['move_distance'] = {
    init: function () {
        this.appendValueInput('distance')
            .setCheck('Number')
            .appendField('posun o ')
        this.appendDummyInput()
            .appendField('úhel')
            .appendField(new Blockly.FieldAngle(90), 'angle')
        this.setInputsInline(true)
        this.setPreviousStatement(true, null)
        this.setNextStatement(true, null)
        this.setStyle('canvas_blocks')
        this.setTooltip(Blockly.Msg["CANVAS_MOVE_DISTANCE"])
        this.setHelpUrl(Blockly.Msg["MOVE_HELP_URL"])
    }
}

Blockly.JavaScript['move_distance'] = function (block) {
    var distance = Blockly.JavaScript.valueToCode(block, 'distance', Blockly.JavaScript.ORDER_ATOMIC)
    var angle = block.getFieldValue('angle')
    return 'moveDistanceCubo(' + distance + ',' + angle + ');\n'
}


Blockly.Blocks['move_distance2'] = {
    init: function () {
        this.appendValueInput("distance")
            .setCheck("Number")
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("posun o");
        this.appendValueInput("angle")
            .setCheck("Number")
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("úhel");
        this.setInputsInline(true);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setStyle('canvas_blocks')
        this.setTooltip(Blockly.Msg["CANVAS_MOVE_DISTANCE"]);
        this.setHelpUrl(Blockly.Msg["MOVE_HELP_URL"]);
    }
};

Blockly.JavaScript['move_distance2'] = function (block) {
    var distance = Blockly.JavaScript.valueToCode(block, 'distance', Blockly.JavaScript.ORDER_ATOMIC);
    var angle = Blockly.JavaScript.valueToCode(block, 'angle', Blockly.JavaScript.ORDER_ATOMIC);
    return 'moveDistance2Cubo(' + distance + ',' + angle + ');\n'
};

Blockly.Blocks['move_select'] = {
    init: function () {
        this.appendDummyInput()
            .appendField('posun do ')
            .appendField(new Blockly.FieldDropdown([
                ['střed', 'CENTER'],
                ['horní pravý roh', 'TRIGHT'],
                ['spodní pravý roh', 'DRIGHT'],
                ['spodní levý roh', 'DLEFT'],
                ['horní levý roh', 'TLEFT']
            ]), 'direction')
        this.setPreviousStatement(true, null)
        this.setNextStatement(true, null)
        this.setStyle('canvas_blocks')
        this.setTooltip(Blockly.Msg["CANVAS_MOVE_SELECT"])
        this.setHelpUrl(Blockly.Msg["MOVE_HELP_URL"])
    }
}

Blockly.JavaScript['move_select'] = function (block) {
    var direction = block.getFieldValue('direction')
    return 'moveSelectCubo("' + direction + '");\n'
}

Blockly.Blocks['actual_position'] = {
    init: function () {
        this.appendDummyInput()
            .appendField('aktuální pozice ')
            .appendField(new Blockly.FieldColour('#ff0000'), 'point_color')
        this.setPreviousStatement(true, null)
        this.setNextStatement(true, null)
        this.setStyle('canvas_blocks')
        this.setTooltip(Blockly.Msg["CANVAS_ACTUAL_POS"])
        this.setHelpUrl(Blockly.Msg["MOVE_HELP_URL"])
    }
}

Blockly.JavaScript['actual_position'] = function (block) {
    var point_color = block.getFieldValue('point_color')
    return 'pointPositionCubo("' + point_color + '");\n'
}

Blockly.Blocks['actual_position_coordinate'] = {
    init: function () {
        this.appendDummyInput()
            .appendField(new Blockly.FieldDropdown([["x", "X"], ["y", "Y"]]), "coordinate")
            .appendField("akt. pozice");
        this.setOutput(true, "Number");
        this.setStyle('canvas_blocks')
        this.setTooltip(Blockly.Msg["CANVAS_ACTUAL_POS_COORDINATE"]);
        this.setHelpUrl(Blockly.Msg["MOVE_HELP_URL"]);
    }
};

Blockly.JavaScript['actual_position_coordinate'] = function (block) {
    var coordinate = block.getFieldValue('coordinate');
    return ['getCoordinateCubo("' + coordinate + '")', Blockly.JavaScript.ORDER_NONE];
};