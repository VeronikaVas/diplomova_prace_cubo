import * as Blockly from 'blockly/core';

Blockly.Msg["LINES_FILL"] = "Nastaví barvu, kterou budou vybarveny tvary";
Blockly.Msg["LINES_STROKE"] = "Nastaví barvu čáry";
Blockly.Msg["LINES_LINE_WIDTH"] = "Nastaví šířku čáry";
Blockly.Msg["LINES_LINE_CAP"] = "Nastaví zakončení čáry";
Blockly.Msg["LINES_LINE"] = "Nakreslí čáru o zadané délce a směru"
Blockly.Msg["LINES_HELP_URL"] = "/napoveda#cary-barvy";

Blockly.Blocks['fill'] = {
    init: function () {
        this.appendValueInput('fillColour')
            .setCheck('Colour')
            .appendField('výplň')
        this.setPreviousStatement(true, null)
        this.setNextStatement(true, null)
        this.setStyle('lines_blocks')
        this.setTooltip(Blockly.Msg["LINES_FILL"])
        this.setHelpUrl(Blockly.Msg["LINES_HELP_URL"])
    }
}

Blockly.JavaScript['fill'] = function (block) {
    var fillcolour = Blockly.JavaScript.valueToCode(block, 'fillColour', Blockly.JavaScript.ORDER_ATOMIC)
    return 'setFillColorCubo(' + fillcolour + ');\n'
}

Blockly.Blocks['stroke'] = {
    init: function () {
        this.appendValueInput('stroke_colour')
            .setCheck('Colour')
            .appendField('barva čáry')
        this.setPreviousStatement(true, null)
        this.setNextStatement(true, null)
        this.setColour(230)
        this.setStyle('lines_blocks')
        this.setTooltip(Blockly.Msg["LINES_STROKE"])
        this.setHelpUrl(Blockly.Msg["LINES_HELP_URL"])
    }
}

Blockly.JavaScript['stroke'] = function (block) {
    var value_stroke_colour = Blockly.JavaScript.valueToCode(block, 'stroke_colour', Blockly.JavaScript.ORDER_ATOMIC)
    return 'setStrokeColorCubo(' + value_stroke_colour + ');\n'
}


Blockly.Blocks['line_width'] = {
    init: function () {
        this.appendValueInput('width')
            .setCheck('Number')
            .appendField('šířka čáry')
        this.setPreviousStatement(true, null)
        this.setNextStatement(true, null)
        this.setColour(230)
        this.setStyle('lines_blocks')
        this.setTooltip(Blockly.Msg["LINES_LINE_WIDTH"])
        this.setHelpUrl(Blockly.Msg["LINES_HELP_URL"])
    }
}

Blockly.JavaScript['line_width'] = function (block) {
    var value_width = Blockly.JavaScript.valueToCode(block, 'width', Blockly.JavaScript.ORDER_ATOMIC)
    return 'setStrokeWidthCubo(' + value_width + ');\n'
}

Blockly.Blocks['line_cap'] = {
    init: function () {
        this.appendDummyInput()
            .appendField('zakončení čáry ')
            .appendField(new Blockly.FieldDropdown([
                ['hranaté', 'BUTT'],
                ['kulaté', 'ROUND']
            ]), 'cap')
        this.setPreviousStatement(true, null)
        this.setNextStatement(true, null)
        this.setStyle('lines_blocks')
        this.setTooltip(Blockly.Msg["LINES_LINE_CAP"])
        this.setHelpUrl(Blockly.Msg["LINES_HELP_URL"])
    }
}

Blockly.JavaScript['line_cap'] = function (block) {
    var dropdown_cap = block.getFieldValue('cap')
    return 'setStrokeCapCubo("' + dropdown_cap + '");\n'
}

Blockly.Blocks['line'] = {
    init: function () {
        this.appendValueInput('distance')
            .setCheck('Number')
            .appendField('čára')
        this.appendDummyInput()
            .appendField('a úhel')
            .appendField(new Blockly.FieldAngle(90), 'angle')
        this.setPreviousStatement(true, null)
        this.setNextStatement(true, null)
        this.setColour(230)
        this.setStyle('lines_blocks')
        this.setTooltip(Blockly.Msg["LINES_LINE"])
        this.setHelpUrl(Blockly.Msg["LINES_HELP_URL"])
    }
}

Blockly.JavaScript['line'] = function (block) {
    var distance = Blockly.JavaScript.valueToCode(block, 'distance', Blockly.JavaScript.ORDER_ATOMIC)
    var angle = block.getFieldValue('angle')
    return 'drawLineCubo(' + distance + ',' + angle + ', true);\n'
}

Blockly.Blocks['line_angle_input'] = {
    init: function () {
        this.appendDummyInput()
            .setAlign(Blockly.ALIGN_CENTRE)
            .appendField('čára')
        this.appendValueInput('distance')
            .setCheck('Number')
        this.appendValueInput('angle')
            .setCheck('Number')
            .appendField('a úhel')
        this.setInputsInline(true)
        this.setPreviousStatement(true, null)
        this.setNextStatement(true, null)
        this.setColour(230)
        this.setStyle('lines_blocks')
        this.setTooltip(Blockly.Msg["LINES_LINE"])
        this.setHelpUrl(Blockly.Msg["LINES_HELP_URL"])
    }
}

Blockly.JavaScript['line_angle_input'] = function (block) {
    var distance = Blockly.JavaScript.valueToCode(block, 'distance', Blockly.JavaScript.ORDER_ATOMIC)
    var angle = Blockly.JavaScript.valueToCode(block, 'angle', Blockly.JavaScript.ORDER_ATOMIC)

    return 'drawLineCubo(' + distance + ',' + angle + ', true);\n'
}