import * as Blockly from 'blockly/core';
Blockly.Msg["SHAPES_RECT"] = "Nakreslí vyplněný/nevyplněný obdélník o zadané šířce a výšce ";
Blockly.Msg["SHAPES_CIRCLE"] = "Nakreslí kruh o zadaném poloměru, kde střed je v aktuální pozici bodu";
Blockly.Msg["SHAPES_ARC"] = "Nakreslí oblouk o zadaném poloměru. Rozsah oblouku je dán zadanými úhly";
Blockly.Msg["SHAPES_PATH"] = "Nakreslí polygon složený ze zadaných čar";
Blockly.Msg["SHAPES_FONT"] = "Nastaví font a velikost vykreslovaného textu";
Blockly.Msg["SHAPES_TEXT"] = 'Nakreslí zadaný text na plátno';
Blockly.Msg["SHAPES_HELP_URL"] = "/napoveda#tvary";

Blockly.Blocks['draw_rect'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("obdélník ");
        this.appendDummyInput()
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField(new Blockly.FieldDropdown([["vyplněno", "FILLED"], ["okraj", "STROKE"]]), "fill");
        this.appendValueInput("height")
            .setCheck("Number")
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField(" výška");
        this.appendValueInput("width")
            .setCheck("Number")
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("šířka");
        this.setInputsInline(true);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setStyle('shapes_blocks')
        this.setTooltip(Blockly.Msg["SHAPES_RECT"]);
        this.setHelpUrl(Blockly.Msg["SHAPES_HELP_URL"]);
    }
};

Blockly.JavaScript['draw_rect'] = function (block) {
    var fill = block.getFieldValue('fill');
    var height = Blockly.JavaScript.valueToCode(block, 'height', Blockly.JavaScript.ORDER_ATOMIC);
    var width = Blockly.JavaScript.valueToCode(block, 'width', Blockly.JavaScript.ORDER_ATOMIC);
    return 'drawRectCubo("' + fill + '",' + width + ',' + height + ');\n'
};

Blockly.Blocks['draw_circle'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("kruh ")
            .appendField(new Blockly.FieldDropdown([["vyplněno", "FILLED"], ["okraj", "STROKE"]]), "fill");
        this.appendValueInput("r")
            .setCheck("Number")
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField(" poloměr ");
        this.setInputsInline(true);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setStyle('shapes_blocks')
        this.setTooltip(Blockly.Msg["SHAPES_CIRCLE"]);
        this.setHelpUrl(Blockly.Msg["SHAPES_HELP_URL"]);
    }
}

Blockly.JavaScript['draw_circle'] = function (block) {
    var fill = block.getFieldValue('fill');
    var r = Blockly.JavaScript.valueToCode(block, 'r', Blockly.JavaScript.ORDER_ATOMIC);

    return 'drawCircleCubo("' + fill + '",' + r + ');\n'
};

Blockly.Blocks['draw_arc'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("oblouk ")
            .appendField(new Blockly.FieldDropdown([["vyplněno", "FILLED"], ["okraj", "STROKE"]]), "fill");
        this.appendValueInput("r")
            .setCheck("Number")
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("poloměr ");
        this.appendDummyInput()
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("od ")
            .appendField(new Blockly.FieldAngle(200), "from");
        this.appendDummyInput()
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField(" do ")
            .appendField(new Blockly.FieldAngle(340), "to");
        this.setInputsInline(true);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setStyle('shapes_blocks')
        this.setTooltip(Blockly.Msg["SHAPES_ARC"]);
        this.setHelpUrl(Blockly.Msg["SHAPES_HELP_URL"]);
    }
};

Blockly.JavaScript['draw_arc'] = function (block) {
    var fill = block.getFieldValue('fill');
    var r = Blockly.JavaScript.valueToCode(block, 'r', Blockly.JavaScript.ORDER_ATOMIC);
    var from = block.getFieldValue('from');
    var to = block.getFieldValue('to');
    return 'drawArcCubo("' + fill + '",' + r + ',' + from + ',' + to + ');\n'
};

Blockly.Blocks['draw_arc_2'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("oblouk ")
            .appendField(new Blockly.FieldDropdown([["vyplněno", "FILLED"], ["okraj", "STROKE"]]), "fill");
        this.appendValueInput("r")
            .setCheck("Number")
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("poloměr ");
        this.appendValueInput("from")
            .setCheck("Number")
            .appendField("od");
        this.appendValueInput("to")
            .setCheck("Number")
            .appendField("do");
        this.setInputsInline(true);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setStyle('shapes_blocks')
        this.setTooltip(Blockly.Msg["SHAPES_ARC"]);
        this.setHelpUrl(Blockly.Msg["SHAPES_HELP_URL"]);
    }
};

Blockly.JavaScript['draw_arc_2'] = function (block) {
    var fill = block.getFieldValue('fill');
    console.log(fill)
    var r = Blockly.JavaScript.valueToCode(block, 'r', Blockly.JavaScript.ORDER_ATOMIC);
    var from = Blockly.JavaScript.valueToCode(block, 'from', Blockly.JavaScript.ORDER_ATOMIC);
    var to = Blockly.JavaScript.valueToCode(block, 'to', Blockly.JavaScript.ORDER_ATOMIC);
    return 'drawArcCubo("' + fill + '",' + r + ',' + from + ',' + to + ');\n'
};


Blockly.Blocks['path'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("polygon ")
            .appendField(new Blockly.FieldDropdown([["vyplněno", "FILLED"], ["okraj", "STROKE"]]), "fill");
        this.appendStatementInput("lines")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("čáry");
        this.setInputsInline(true);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setStyle('shapes_blocks')
        this.setTooltip(Blockly.Msg["SHAPES_PATH"]);
        this.setHelpUrl(Blockly.Msg["SHAPES_HELP_URL"]);
    }
};

String.prototype.replaceAll = function (search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
}

Blockly.JavaScript['path'] = function (block) {
    var fill = block.getFieldValue('fill');
    var lines = Blockly.JavaScript.statementToCode(block, 'lines');
    lines = lines.replaceAll('true', 'false');
    var code = 'openPathCubo();\n' + lines + 'closePathCubo("' + fill + '");\n';
    return code;
}

Blockly.Blocks['font'] = {
    init: function () {
        this.appendDummyInput()
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField('font ')
            .appendField(new Blockly.FieldDropdown([
                ['Arial', 'ARIAL'],
                ['Times', 'TIMES'],
                ['Courier', 'COURIER']
            ]), 'font')
        this.appendValueInput('size')
            .setCheck('Number')
            .appendField(' velikost')
        this.setInputsInline(true)
        this.setPreviousStatement(true, null)
        this.setNextStatement(true, null)
        this.setColour('#546E7A')
        this.setTooltip(Blockly.Msg["SHAPES_FONT"])
        this.setHelpUrl(Blockly.Msg["SHAPES_HELP_URL"])
    }
}

Blockly.JavaScript['font'] = function (block) {
    var font = block.getFieldValue('font')
    var size = Blockly.JavaScript.valueToCode(block, 'size', Blockly.JavaScript.ORDER_ATOMIC)

    return 'setFontCubo("' + font + '",' + size + ');\n'
}

Blockly.Blocks['draw_text'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("text ")
            .appendField(new Blockly.FieldDropdown([["vyplněno", "FILLED"], ["okraj", "STROKE"]]), "fill");
        this.appendValueInput("text")
            .setCheck("String")
            .setAlign(Blockly.ALIGN_RIGHT)
        this.setInputsInline(true);
        this.setPreviousStatement(true, null)
        this.setNextStatement(true, null)
        this.setColour('#546E7A')
        this.setTooltip(Blockly.Msg["SHAPES_TEXT"])
        this.setHelpUrl(Blockly.Msg["SHAPES_HELP_URL"])
    }
}

Blockly.JavaScript['draw_text'] = function (block) {
    var fill = block.getFieldValue('fill');
    var text = Blockly.JavaScript.valueToCode(block, 'text', Blockly.JavaScript.ORDER_ATOMIC)

    return 'drawTextCubo("' + fill + '",' + text + ');\n'
}