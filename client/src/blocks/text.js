/*eslint-disable */
import * as Blockly from 'blockly/core';

Blockly.Blocks['cubo_text_charat'] = {
    init: function () {
        this.appendDummyInput()
            .appendField('v textu')
        this.appendValueInput('VALUE')
            .setCheck('String')
        this.appendDummyInput()
            .appendField('získej písmeno na pozici')
        this.appendValueInput('AT')
            .setCheck('Number')
        this.setOutput(true, null)
        this.setColour(230)
        this.setStyle('text_blocks')
        this.setTooltip('Získá ze zadaného textu písmeno určené pozicí. Pozice s hodnotou 1 vrátí 1. znak textu.')
        this.setHelpUrl('/napoveda#text')
    }
}

Blockly.JavaScript['cubo_text_charat'] = function (block) {
    var value = Blockly.JavaScript.valueToCode(block, 'VALUE', Blockly.JavaScript.ORDER_ATOMIC)
    var at = Blockly.JavaScript.valueToCode(block, 'AT', Blockly.JavaScript.ORDER_ATOMIC)
    at -= 1
    var code = value + '.charAt(' + at + ')'
    return [code, Blockly.JavaScript.ORDER_FUNCTION_CALL]
}

Blockly.Blocks['cubo_text_substring'] = {
    init: function () {
        this.appendDummyInput()
            .appendField('v textu')
        this.appendValueInput('VALUE')
            .setCheck('String')
        this.appendDummyInput()
            .appendField('získat podřetězec od')
        this.appendValueInput('FROM')
            .setCheck('Number')
        this.appendDummyInput()
            .appendField('do')
        this.appendValueInput('TO')
            .setCheck('Number')
        this.setOutput(true, null)
        this.setColour(230)
        this.setStyle('text_blocks')
        this.setTooltip('Získá zadanou část textu zadanou pozicemi. První znak má pozici 1')
        this.setHelpUrl('/napoveda#text')
    }
}

Blockly.JavaScript['cubo_text_substring'] = function (block) {
    var value = Blockly.JavaScript.valueToCode(block, 'VALUE', Blockly.JavaScript.ORDER_ATOMIC)
    var from = Blockly.JavaScript.valueToCode(block, 'FROM', Blockly.JavaScript.ORDER_ATOMIC)
    var to = Blockly.JavaScript.valueToCode(block, 'TO', Blockly.JavaScript.ORDER_ATOMIC)
    from -= 1
    var code = value + '.slice(' + from + ', ' + to + ')'
    return [code, Blockly.JavaScript.ORDER_FUNCTION_CALL]
}
/*eslint-enable */