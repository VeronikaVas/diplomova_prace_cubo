import * as Blockly from 'blockly/core';

var CONTROL_LOCAL_DEFINITION_MIXIN = {

    TYPES: ['procedures_defnoreturn', 'procedures_defreturn'],
    FCE: ['value'],
    onchange: function (/* e */) {
        if (!this.workspace.isDragging || this.workspace.isDragging()) {
            return // Don't change state at the start of a drag.
        }
        var legal = false

        var block = this
        do {
            if (this.TYPES.indexOf(block.type) != -1) {
                //fff = block.id
                legal = true
                break
            }
            block = block.getSurroundParent()
        } while (block)
        if (legal) {
            this.setWarningText(null)
            this.FCE[0] = block.id
            if (!this.isInFlyout) {
                // this.setEnabled(true)
                this.setDisabled(false)
            }
        } else {
            this.setWarningText('Nastavení lokální proměnné musí být umístěno v bloku funkce.')

            if (!this.isInFlyout && !this.getInheritedDisabled()) {
                // this.setEnabled(true)
                this.setDisabled(true)
            }
        }
    }
}

Blockly.Extensions.registerMixin('local_definition_check',
    CONTROL_LOCAL_DEFINITION_MIXIN)

Blockly.defineBlocksWithJsonArray([
    {
        'type': 'local_definition',
        'message0': 'použít  %1 jako lokální proměnnou',
        'args0': [
            {
                'type': 'field_variable',
                'name': 'NAME',
                'variable': null
            }
        ],
        'previousStatement': null,
        'nextStatement': null,
        'style': 'variable_blocks',
        'tooltip': 'Vybranou proměnnou označí jako lokální (zastíní ji), musí být zanořena v bloku',
        'helpUrl': '/napoveda#promenne',
        'extensions': ['local_definition_check']
    }
])

Blockly.JavaScript['local_definition'] = function (block) {
    var variable_name = Blockly.JavaScript.variableDB_.getName(block.getFieldValue('NAME'), Blockly.Variables.NAME_TYPE)
    var code = 'var ' + variable_name + ';\n'
    return code
}