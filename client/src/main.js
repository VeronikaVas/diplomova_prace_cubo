import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import VeeValidate from "vee-validate";

Vue.component("default-layout", () => import("./views/layout/Default.vue"))
Vue.component("app-layout", () => import("./views/layout/CuboApp.vue"))
Vue.component("ide-layout", () => import("./views/layout/IDE.vue"))

VeeValidate.Validator.localize('en', {
  custom: {
    email: {
      required: "E-mailová adresa nemůže být prázdná",
      email: "E-mailová adresa je ve špatném tvaru"

    },
    password: {
      min: "Heslo musí obsahovat alespoň 8 znaků.",
      required: "Heslo nemůže být prázdné.",
      regex: "Heslo musí obsahovat alespoň jedno velké písmeno, malé písmeno, číslici a speciální znak."
    },
    passwordConfirm: {
      min: "Heslo musí obsahovat alespoň 8 znaků.",
      required: "Heslo nemůže být prázdné."
    },
    contactName: {
      required: "Jméno odesílatele nemůže být prázdné",
      min: "Jméno musí obsahovat alespoň 3 znaky.",
      alpha_dash: "Jméno může obsahovat pouze abecední znaky, čísla, pomlčky nebo podtržítka"

    },
    contactMessage: {
      required: "Zpráva nemůže být prázdná.",
      min: "Zpráva musí mít alespoň 10 znaků"
    },
    nickname: {
      required: "Přezdívka nemůže být prázdná",
      min: "Přezdívka musí mít nejméně 3 znaky",
      max: "Přezdívka může mít maximálně 30 znaků"
    },
    taskName: {
      required: "Název úkolu nemůže být prázdný.",
      min: "Název úkolu musí obsahovat alespoň 3 znaky.",
      max: "Název úkolu může obsahovat maximálně 30 znaků.",
      alpha_dash: "Název úkolu může obsahovat jen alfanumerické znaky, podtržítka a pomlčky"
    },
    collectionkName: {
      required: "Název kolekce nemůže být prázdný.",
      min: "Název kolekce musí obsahovat alespoň 3 znaky.",
      max: "Název kolekce může obsahovat maximálně 30 znaků."
    }
  }
})


Vue.use(VeeValidate);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");


