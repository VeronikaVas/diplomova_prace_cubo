import store from '../store/index' // your vuex store
export default {

  ifAuthenticated (to, from, next) {
    if (store.getters['user/isAuthenticated']) {
      next()
      return
    }
    next('/prihlaseni')
  }
}
