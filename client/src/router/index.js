import Vue from "vue";
import VueRouter from "vue-router";

import usersRoutes from './user'
import tasksRoutes from './task'

Vue.use(VueRouter);

const routes = [
  ...usersRoutes,
  ...tasksRoutes,
  {
    path: "/",
    name: "Home",
    meta: {
      layout: "default",
      title: "Cubo"
    },
    component: () =>
      import("../views/Home.vue")
  },
  {
    path: "/napoveda",
    name: "Help",
    meta: {
      layout: "default",
      title: "Nápověda | Cubo"
    },
    component: () =>
      import("../views/Help.vue")
  },
  {
    path: "/kontakt",
    name: "Conatct",
    meta: {
      layout: "default",
      title: "Kontakt | Cubo"
    },
    component: () =>
      import("../views/mail/Contact.vue")
  },
  {
    path: "/prihlaseni",
    name: "Login",
    meta: {
      layout: "default",
      title: "Přihlášení"
    },
    component: () =>
      import("../views/auth/Login.vue")
  },
  {
    path: "/registrace",
    name: "SignUp",
    meta: {
      layout: "default",
      title: "Registrace"
    },
    component: () =>
      import("../views/auth/SignUp.vue")
  },

  {
    path: "/vyvojove-prostredi",
    name: "IDE",
    meta: {
      layout: "ide",
      title: "Vývojové prostředí | Cubo"
    },
    component: () =>
      import("../views/layout/IDE.vue")
  },

  {
    path: '*',
    meta: { layout: 'default', title: 'Stránka nenalezena | Cubo' },
    component: () => import('../views/NotFound.vue')

  }

];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

router.beforeEach((to, from, next) => {
  document.title = to.meta.title;
  next();
})

export default router;
