import guard from './guard.js'

export default [
    {
        path: "/ukoly",
        name: "Tasks",
        meta: {
            layout: "app",
            title: "Úkoly | Cubo"
        },
        beforeEnter: guard.ifAuthenticated,
        component: () =>
            import("../views/app/task/Tasks.vue")
    },

    {
        path: "/vytvoreni-ukolu",
        name: "AddTasks",
        meta: {
            layout: "app",
            title: "Nový úkol | Cubo"
        },
        beforeEnter: guard.ifAuthenticated,
        component: () =>
            import("../views/app/task/AddEditTask.vue")
    },
    {
        path: "/editace-ukolu/:id",
        name: "EditTask",
        meta: {
            layout: "app",
            title: "Úprava úkolu | Cubo"
        },
        beforeEnter: guard.ifAuthenticated,
        component: () =>
            import("../views/app/task/AddEditTask.vue")
    },

    {
        path: "/ukoly/:id",
        name: "ExerciseTask",
        meta: {
            layout: "app",
            title: "Procvičování | Cubo"
        },
        props: { default: true, limit: true },
        beforeEnter: guard.ifAuthenticated,
        component: () =>
            import("../views/app/task/PracticeTask.vue")
    },

]
