import guard from './guard.js'

export default [
    {
        path: "/prehled",
        name: "Dashboard",
        meta: {
            layout: "app",
            title: "Přehled | Cubo"
        },
        beforeEnter: guard.ifAuthenticated,
        component: () =>
            import("../views/app/Dashboard.vue")
    },
    {
        path: '/nastaveni-profilu',
        name: 'user-settings',
        meta: { layout: 'app', title: 'Nastavení | Cubo' },
        beforeEnter: guard.ifAuthenticated,
        component: () => import('../views/app/UserSettings.vue')
    },
]
