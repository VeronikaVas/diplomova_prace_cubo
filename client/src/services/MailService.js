import Api from '@/services/Api'

export default {
    send(credentials) {
        return Api().post('api/send', credentials)
    },
}