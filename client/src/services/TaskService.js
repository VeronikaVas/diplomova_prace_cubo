import Api from '@/services/Api'

export default {
    getAll(credentials) {
        return Api().get('api/tasks', credentials)
    },

    create(credentials) {
        return Api().post('api/tasks', credentials)
    },

    edit(credentials) {
        return Api().put('api/tasks', credentials)
    },

    delete(credentials) {
        var url = `api/tasks/${credentials.id}`
        return Api().delete(url)
    },

    task(credentials) {
        var url = `api/tasks/${credentials.id}`
        return Api().get(url)
    },

    solution(credentials) {
        return Api().post('api/solution', credentials)
    },
}