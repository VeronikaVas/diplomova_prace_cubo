import Api from '@/services/Api'

export default {

    changePassword(credentials) {
        return Api().put('/api/user/password', credentials)
    },
    deactivate(credentials) {
        return Api().delete('/api/user', credentials)
    },
    getInfo(credentials) {
        return Api().get('/api/user/info', credentials)
    }



}