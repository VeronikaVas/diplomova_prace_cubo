import Vue from "vue";
import Vuex from "vuex";

import editor from './modules/editor.js'
import user from './modules/user.js'
import loading from './modules/loading.js'

Vue.use(Vuex);

export default new Vuex.Store({
  modules: { editor, user, loading }
})