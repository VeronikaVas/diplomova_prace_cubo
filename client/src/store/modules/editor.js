export default {
  namespaced: true,

  state: {
    openPanel: false,
    saving: true,
    runOpening: true,
    workspace: null,
    output: [],
    canvas: null,
    image: null,
    canvasActions: []
  },

  mutations: {
    openPanel(state, l) {
      state.openPanel = l
    },
    setSaving(state, l) {
      state.saving = l
    },
    setOpening(state, l) {
      state.runOpening = l
    },
    setWorkspace(state, w) {
      state.workspace = w
    },
    addToOutput(state, l) {
      state.output.push(l)
    },
    setCanvas(state, l) {
      state.canvas = l
    },
    clearOutput(state) {
      state.output = []
    },
    setImage(state, l) {
      state.image = l
    }
  },
  actions: {
    setWorkspace({ commit }, w) {
      commit('setWorkspace', w)
    }
  },
  getters: {
    openPanel: state => state.openPanel,
    saving: state => state.saving,
    runOpening: state => state.runOpening,
    workspace: state => state.workspace,
    output: state => state.output,
    canvas: state => state.canvas,
    imageCanvas: state => state.image
  }
}
