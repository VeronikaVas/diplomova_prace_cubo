export default {
    namespaced: true,

    state: {
        showLoading: false
    },

    mutations: {
        showLoading(state, value) {
            state.showLoading = value;
        },
    },

    getters: {
        showLoading: (state) => state.showLoading
    }
}