import axios from "axios"


export default {
  namespaced: true,
  state: {
    token: window.localStorage.getItem("user-token") || "",
    status: "",
    user: JSON.parse(localStorage.getItem("user")) || null
  },
  mutations: {
    setUser(state, user) {
      state.user = user;
      localStorage.user = JSON.stringify(user);
    },
    updateUser(state, u) {
      var user = state.user;
      user.nickname = u.nickname;
      user.profile.firstName = u.first;
      user.profile.lastName = u.last;

      state.user = user;
      localStorage.user = JSON.stringify(user);
    },

    loadSettings(state) {
      var u = localStorage.user;
      if (u) {
        state.user = JSON.parse(u);
        state.token = localStorage.token;
      }
    },
    authRequest: (state) => {
      state.status = "loading";
    },
    authSuccess: (state, token) => {
      state.status = "success";
      state.token = token;
    },
    authError: (state) => {
      state.status = "error";
    },
    logout: (state) => {
      state.token = null;
      state.user = null;
      state.status = "";
    }

  },
  actions: {
    setToken({ commit }, token) {
      commit("setToken", token);
    },
    setUser({ commit }, user) {
      commit("setUser", user);
    },
    /*eslint-disable */
    login: ({ commit, dispatch }, user) => {
      return new Promise((resolve, reject) => {
        commit("authRequest");
        
      })
    },

    logout: ({ commit, dispatch }) => {
      return new Promise((resolve, reject) => {
        commit("logout");
        localStorage.removeItem("user-token");
        localStorage.removeItem("user");

        // odstraneni - axios default header
        delete axios.defaults.headers.common["Authorization"]
        resolve()
      })
    },
    /*eslint-enable */
    check({ commit }) {
      commit("loadSettings")
    }
  },
  getters: {
    user: state => state.user,
    isAuthenticated: state => !!state.token,
    authStatus: state => state.status,
    getToken: state => state.token

  }

}
