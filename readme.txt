Spuštění klienta
----------------------------------------------------------------
npm install 
npm run serve

Spuštění serveru
----------------------------------------------------------------
npm install
node app.js

Generování knihovny
----------------------------------------------------------------
Při změně v knihovně je nutné přegenerovat pomocí
python build.py

Po přegenerování uděláme export souborů pro použití jako balíčku
npm run package

a následně ve složce dist/
npm pack

což vytvoří lokální balíček.


Aplikace je přístupná pro testování na https://kmi-cubo.herokuapp.com/

Použité balíčky vždy v souboru package.json, ikony jsou dány do jednoho fontu pomocí služby IcoMoon.
