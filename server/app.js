const express = require('express')
const config = require('./config/config')
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');
var morgan = require('morgan')

const passport = require('passport');
const app = express()


/**
 * Connect to MongoDB.
 */
mongoose.Promise = require('bluebird');
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);
mongoose.set('useUnifiedTopology', true);
mongoose.set('useNewUrlParser', true);
mongoose.connect(config.MONGODB_URI, { promiseLibrary: require('bluebird') });
mongoose.connection.on('error', (err) => {
    console.error(err);
    console.log('%s MongoDB connection error. Please make sure MongoDB is running.', '✗');
    process.exit();
});

// Middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ 'extended': 'false' }));

app.use(cors());
app.use(morgan('dev'));
app.use(passport.initialize());

require('./routes')(app);
require('./passport')

// Handle production
if (process.env.NODE_ENV === 'production') {
    // Static folder
    app.use(express.static(__dirname + '/public/'));
    // Handle SPA
    app.get(/.*/, (req, res) => res.sendFile(__dirname + '/public/index.html'));
}

app.listen(config.port, () => {
    console.log('%s App is running at http://localhost:%d in %s mode', '✓', config.port, app.get('env'));
    console.log('  Press CTRL-C to stop\n');
});