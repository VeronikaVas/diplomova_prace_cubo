module.exports = {
    port: process.env.PORT || 8081,
    MONGODB_URI: 'mongodb://',
    authentication: {
      jwtSecret: process.env.JWT_SECRET || 'nejakeZvoleneTajemstvi'
    }
  }