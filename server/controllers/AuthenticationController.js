const User = require('../models/User')
const jwt = require('jsonwebtoken')
const validator = require('validator')
const config = require('../config/config')

function jwtSignUser(user) {
    const ONE_WEEK = 60 * 60 * 24 * 7
    return jwt.sign(user, config.authentication.jwtSecret, {
        expiresIn: ONE_WEEK
    })
}

module.exports = {
    async register(req, res) {
        const validationErrors = [];
        if (!validator.isEmail(req.body.email)) validationErrors.push('Neplatná emailová adresa.');
        if (!validator.isLength(req.body.password, { min: 8 })) validationErrors.push('Heslo musí mít nejméně 8 znaků.');
        if (req.body.password !== req.body.confirmPassword) validationErrors.push('Hesla se neshodují.');
        if (!validator.isLength(req.body.nickname, { min: 3, max: 30 })) validationErrors.push('Přezdívka musí mít 3 až 30 znaků.');
        if (validationErrors.length) {
            return res.status(422).send(validationErrors);
        }
        if (!validator.isEmail(req.body.email))
            req.body.email = validator.normalizeEmail(req.body.email, { gmail_remove_dots: false })

        var newUser = new User({
            email: req.body.email,
            password: req.body.password,
            nickname: req.body.nickname
        });
        // save the user
        newUser.save(function (err) {
            if (err) {
                var field = err.errmsg.split('.$')[1]
                if (err.code === 11000) {
                    if (field.includes("nickname"))
                        return res.status(422).send(['Tato přezdívka již existuje.']);
                    else
                        return res.status(422).send(['Tento email je již používán.']);
                }
                return res.status(422).send(err);
            }
            return res.json('Uživatel vytvořen.');
        });

    },
    login(req, res) {

        User.findOne({
            email: req.body.email,
            active: true
        }, function (err, user) {
            if (err) throw err;

            if (!user) {
                res.status(401).send({
                    success: false,
                    msg: 'Špatné heslo nebo email.'
                });
            } else {
                // check if password matches
                user.comparePassword(req.body.password, function (err, isMatch) {
                    if (isMatch && !err) {
                        // if user is found and password is right create a token
                        const userJson = { "_id": user._id, "nickname": user.nickname, "profile": user.profile }

                        // return the information including token as JSON
                        res.json({
                            success: true,
                            user: userJson,
                            token: jwtSignUser(userJson)
                        });
                    } else {
                        res.status(401).send({
                            success: false,
                            msg: 'Špatné heslo nebo email.'
                        });
                    }
                });
            }
        });

    }
}