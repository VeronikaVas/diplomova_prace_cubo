
var nodeMailer = require('nodemailer')
const validator = require('validator')

module.exports = {
    sendEmail(req, res) {
        const validationErrors = [];
        if (!validator.isEmail(req.body.email))
            return res.status(400).send({ origin: 'email', error: 'Neplatná emailová adresa.' });
        if (!validator.isLength(req.body.name, { min: 3 }))
            return res.status(400).send({ origin: 'name', error: 'Zadané jméno je příliš krátké (min. 3 znaky).' });

        if (!validator.isLength(req.body.name, { max: 35 }))
            return res.status(400).send({ origin: 'name', error: 'Zadané jméno je příliš dlouhé (max. 35 znaků).' });



        if (!validator.isLength(req.body.message, { min: 10 }))
            return res.status(400).send({ origin: 'message', error: 'Zpráva musí mít alespoň 10 znaků.' });

        if (!validator.isLength(req.body.message, { max: 1000 }))
            return res.status(400).send({ origin: 'message', error: 'Zpráva nesmí více než 1000 znaků.' });


        if (!validator.isEmail(req.body.email))
            req.body.email = validator.normalizeEmail(req.body.email, { gmail_remove_dots: false })

        let transporter = nodeMailer.createTransport({
            host: 'smtp.gmail.com',
            port: 465,
            secure: true,
            auth: {
                user: 'xxx@gmail.com',
                pass: 'xxx'
            },
            tls: {
                rejectUnauthorized: false
            }
        });

        var text = '<b>' + req.body.name + '<br />' + req.body.email + '</b><p>' + req.body.message + '</p>';

        let mailOptions = {

            to: '"Veronika Vašinová" <vasinova.ver@gmail.com>',
            subject: 'Dotaz - Cubo', // Subject line
            html: text// html body
        };

        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                return console.log(error);
            }
            console.log('Message %s sent: %s', info.messageId, info.response);
            res.render('index');
        });


        return res.status(200).send('Odesláno.');
    }
}