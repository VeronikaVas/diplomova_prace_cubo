const Task = require('../models/Task')
const Solution = require('../models/Solution')
const User = require('../models/User')
const validator = require('validator')
var ObjectId = require('mongodb').ObjectId;
module.exports = {
    async getAll(req, res) {
        var s = req.query.search.trim() || '';
        var completed = req.query.state;
        var difficulty = req.query.difficulty || '';
        var type = req.query.type || '';
        var order = req.query.order || '';
        var query = {
            'name': {
                "$regex": s,
                "$options": "i"
            },
            'valid': true
        }

        if (type != '') {
            query['type'] = type;
        }

        if (difficulty != '') {
            query['difficulty'] = difficulty;
        }
        var s = await Solution.find({ user: req.user._id }).select('-_id task')
        var solutions = s.map(function (item) {
            return new ObjectId(item['task']);
        });

        if (completed === 'not_complete' || completed === 'complete') {
            try {
                var s = await Solution.find({ user: req.user._id }).select('_id task')

                console.log(solutions)

                if (completed === 'not_complete') {
                    query['user'] = { $ne: req.user._id }
                    query['_id'] = { $nin: solutions }
                }
                else {
                    query['user'] = { $ne: req.user._id }
                    query['_id'] = { $in: solutions }
                }
            } catch (err) {
                console.log(err)
            }
        }
        if (completed === 'user') {
            query['user'] = req.user._id;
        }

        //trideni
        var sort = { createdAt: 'desc' }
        switch (req.query.order) {
            case 'name_asc':
                sort = { name: 'asc' };
                break;
            case 'name_desc':
                sort = { name: 'desc' };
                break;

            case 'created_at_desc':
                sort = { createdAt: 'desc' };
                break;;

            case 'created_at_asc':
                sort = { createdAt: 'asc' };
                break
        }

        var params = req.query;
        var limit = validator.isNumeric(params.limit) ? params.limit : 20;
        var offset = validator.isNumeric(params.page) ? params.page * limit - limit : 0;

        var options = {
            select: '_id name user points type difficulty',
            populate:
            {
                path: 'user',
                select: '_id nickname points'
            },
            sort: sort,
            collation: { locale: "cs" },
            lean: true,
            offset: offset,
            limit: limit
        };

        Task.paginate(query, options).then(function (result) {
            result.docs.forEach(element => {
                arr = s.filter(function (item) {
                    return (item.task == element["_id"]);
                });

                let r = s.some(val => {
                    console.log(val)
                    return typeof val["task"] == element["_id"];
                });
                console.log(r)

                /*if () {
                    console.log("yes")
                }*/
            });

            return res.send(result)
        });
    },

    async task(req, res) {
        try {
            let task = await Task.findById(req.params.id).lean()
            let countPoints = true;
            if (req.user._id == task.user) {
                countPoints = false;
            } else {
                let sol = await Solution.find({ user: req.user._id, task: req.params.id }).lean();
                if (sol.length > 0) {
                    countPoints = false;
                }
            }

            task.instructions = validator.unescape(task.instructions)
            task.name = validator.unescape(task.name)
            return res.send({ task, countPoints });
        } catch (err) {
            res.status(403).send({
                msg: 'Úkol se nepodařilo získat.'
            });
        }
    },

    remove(req, res) {
        Task.updateOne({
            _id: req.params.id,
            user: req.user
        }, {
            valid: false
        }, function (err, r) {
            if (err)
                return res.status("403").send("Úkol nebyl odstraněn.")
            return res.status(200).send('Úkol odstraněn.'
            );
        });

    },


    createTask(req, res) {
        var validError = "";

        if (!validator.isLength(req.body.name, { min: 3 }))
            return res.status(400).send({ origin: 'name', error: 'Název musí obsahovat alespoň 3 znaky.' });

        if (!validator.isLength(req.body.name, { max: 30 }))
            return res.status(400).send({ origin: 'name', error: 'Název může obsahovat maximálně 30 znaků.' });

        if (!validator.isLength(req.body.instructions, 0))
            return res.status(400).send({ origin: 'instructions', error: 'Zadání nemůže být prázdné.' });
        var name = validator.escape(req.body.name.trim())
        var instruction = validator.escape(req.body.instructions.trim());

        const task = new Task({
            name: name,
            points: req.body.difficulty,
            difficulty: req.body.difficulty,
            type: req.body.type,
            instructions: instruction,
            codeResult: req.body.codeResult,
            blocksResult: req.body.blocksResult,
            functionsDef: req.body.functionsDef || '',
            calls: req.body.calls || '',
            user: req.user._id,
            canvasResult: req.body.canvasResult || ''
        });
        task.save(function (err, t) {
            if (err) return res.status(403).send('Úkol nevytvořen.');
            return res.status(200).send('Úkol vytvořen.');

        });

    },

    editTask(req, res) {
        if (!validator.isLength(req.body.name, { min: 3 }))
            return res.status(400).send({ origin: 'name', error: 'Název musí obsahovat alespoň 3 znaky.' });

        if (!validator.isLength(req.body.name, { max: 30 }))
            return res.status(400).send({ origin: 'name', error: 'Název může obsahovat maximálně 30 znaků.' });

        if (!validator.isLength(req.body.instructions, 0))
            return res.status(400).send({ origin: 'instructions', error: 'Zadání nemůže být prázdné.' });
        var name = validator.escape(req.body.name.trim())
        var instruction = validator.escape(req.body.instructions.trim());
        console.log(req.body.task)
        Task.findById(req.body.task, (err, tutorial) => {
            if (err) {
                return next(err);
            }
            tutorial.name = name || '';
            //tutorial.result = req.body.result;
            tutorial.instructions = instruction || '';
            tutorial.type = req.body.type || '';
            tutorial.difficulty = req.body.difficulty || '';
            tutorial.codeResult = req.body.codeResult,
                tutorial.blocksResult = req.body.blocksResult,
                tutorial.functionsDef = req.body.functionsDef || ''

            if (tutorial.user = req.user._id) {
                tutorial.save((err) => {
                    if (err) {
                        console.log(err)
                        return res.status(403).send('Úkol se nepodařilo aktualizovat.');
                    }

                    return res.status(200).send('Úkol aktualizován.');
                });
            } else {
                return res.status(403).send('Úkol se nepodařilo aktualizovat, neoprávněná uprava');
            }

        });

    },


    async userCompletedTasks(req, res) {
        try {
            user = await User.findOne({ nickname: req.params.nickname })
            solutions = await Solution.find({ user: user._id }).select('-_id task').lean()
            var taskIds = [];
            for (var i = 0; i < solutions.length; i++) {
                taskIds.push(solutions[i].task);
            }
            console.log(taskIds)
            Task.find({
                '_id': { $in: taskIds }
            }).select('name user type difficulty').populate('user', 'nickname profile.firstName profile.lastName').exec(function (err, tasks) {
                if (err) return res.status(403).send({
                    msg: 'Úkoly se nepodařilo získat.'
                });
                else
                    return res.send({ tasks: tasks, user: user._id });
            });
        } catch (error) {
            return res.status(403).send({
                msg: 'Úkol se nepodařilo získat.'
            });

        }

    },
    async userCreatedTasks(req, res) {
        try {
            user = await User.findOne({ nickname: req.params.nickname })
            var s = req.query.search || ''


            Task.find({
                user: user._id, name: {
                    "$regex": s,
                    "$options": "i"
                }
            }).select('name user type difficulty').populate('user', 'nickname profile.firstName profile.lastName').exec(function (err, tasks) {
                if (err) return res.status(403).send({
                    msg: 'Úkol se nepodařilo získat.'
                });
                else
                    return res.send({ tasks: tasks, user: user._id });

            });

        } catch (error) {
            console.log(error)
            return res.status(403).send({
                msg: 'Úkol se nepodařilo získat.'
            });

        }

    },



}