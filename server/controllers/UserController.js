const User = require('../models/User')
const Task = require('../models/Task')
const Solution = require('../models/Solution')
const validator = require('validator');

module.exports = {

    change(req, res) {
        var password = req.body.password
        var confirmPassword = req.body.confirmPassword

        // Minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character
        var regex = new RegExp("^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{6,})");

        if (!regex.test(password))
            return res.status(400).send({ origin: 'password', error: 'Heslo musí obsahovat alespoň jedno velké písmeno, malé písmeno, číslici a speciální znak.' });

        if (password !== confirmPassword)
            return res.status(400).send({ origin: 'passwordConfirm', error: 'Hesla se neshodují.' });

        User.findById(req.user, (err, user) => {
            if (err) {
                return res.status(403).send({ origin: 'changeError', error: 'Heslo se nepodařilo změnit.' });
            }
            user.password = req.body.password;
            user.save((err, user) => {
                if (err) {
                    return res.status(403).send({ origin: 'changeError', error: 'Heslo se nepodařilo změnit.' })
                }

                return res.status(200).send({ origin: 'passwordChange', msg: 'Heslo změněno.' });
            });
        });
    },





    deactivate(req, res) {
        User.findById(req.user, (err, user) => {
            if (err) {
                return res.status(403).send('Profil uživatele nenalezen.');
            }
            user.active = false;

            user.save((err) => {
                if (err) {
                    return res.status(422).send('Profil se nepodařilo deaktivovat');
                }
                return res.status(200).send('Profil byl deaktivován.');
            });
        });
    },



    async statistics(req, res) {
        try {
            user = await User.findById(req.user).select('points -_id')
            createdTasks = await Task.find({ user: req.user }).select('_id')
            completedTasks = await Solution.find({ user: req.user }).select('_id')

            let lastCreatedTasks = await Task.find({ user: req.user, valid: true }).select('_id name difficulty type user')
                .populate({
                    path: 'user',
                    select: '_id nickname points'
                }).sort({
                    createdAt: 'desc'
                }).limit(5)

            let lastCompletedTasks = await Solution.find({ user: req.user }).populate({
                path: 'task',
                populate: {
                    path: 'user',
                    select: '_id nickname points'
                },
                select: "_id name type difficulty user"
            }).sort({ createdAt: 'desc' }).limit(5)

            console.log(lastCompletedTasks)


            let data = { points: user.points, createdTasks: createdTasks.length, completedTasks: completedTasks.length }
            return res.status(200).send({ data, lastCreatedTasks, lastCompletedTasks });
        } catch (error) {
            return res.status(403).send('Informace se nepodařilo získat');
        }
    },

}