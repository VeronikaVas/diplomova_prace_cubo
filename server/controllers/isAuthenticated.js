const passport = require('passport')

module.exports = function (req, res, next) {
  passport.authenticate('jwt', { session: false }, function (err, user) {
    if (err || !user) {
      res.status(403).send({
        error: 'K tomuto endpointu nemáte přístup!'
      })
    } else {
      req.user = user._id
      next()
    }
  })(req, res, next)
}