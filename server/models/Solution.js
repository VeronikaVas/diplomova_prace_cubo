const mongoose = require('mongoose');
var ObjectId = mongoose.Schema.Types.ObjectId;

const solutionSchema = new mongoose.Schema({
  user: { type: ObjectId, ref: 'User', index: true },
  task: { type: ObjectId, ref: 'Task' }

}, { timestamps: true });

const Solution = mongoose.model('Solution', solutionSchema);

module.exports = Solution;