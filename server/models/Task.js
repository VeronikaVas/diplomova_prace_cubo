const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

var ObjectId = mongoose.Schema.Types.ObjectId;

const taskSchema = new mongoose.Schema({
  name: {
    type: String,
    maxlength: [30, 'Název může obsahovat maximálně 30 znaků.'],
    minlength: [3, 'Název musí obsahovat alespoň 3 znaky.'],
  },
  user: { type: ObjectId, ref: 'User' },
  points: Number,
  type: {
    type: String,
    enum: ['MATCH', 'FUNCTION', 'DRAWING'],
    default: 'MATCH'
  },
  difficulty: { type: Number, default: 1 },
  description: String,
  instructions: Object,
  blocksResult: String,
  codeResult: String,

  /* for functions */
  functionsDef: String,

  /* for drawing */
  canvasResult: Object,
  valid: { type: Boolean, default: true }


}, { timestamps: true });

taskSchema.plugin(mongoosePaginate);

const Task = mongoose.model('Task', taskSchema);

module.exports = Task;