const mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');
var ObjectId = mongoose.Schema.Types.ObjectId;

const userSchema = new mongoose.Schema({
    email: {
        type: String,
        unique: true,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    nickname: {
        type: String,
        maxlength: [15, 'Přezdívka může obsahovat maximálně 15 znaků.'],
        minlength: [3, 'Přezdívka musí obsahovat alespoň 3 znaky.'],
        unique: true,
        required: true,
        index: true
    },
    points: {
        type: Number,
        default: 0
    },
    collections: {
        type: Number,
        default: 0
    },
    completed: {
        type: Number,
        default: 0
    },
    created: {
        type: Number,
        default: 0
    },
    active: {
        type: Boolean,
        default: true
    }

}, {
    timestamps: true
});

/**
 * Password hash middleware.
 */
userSchema.pre('save', function save(next) {
    const user = this;
    if (!user.isModified('password')) {
        return next();
    }
    bcrypt.genSalt(10, (err, salt) => {
        if (err) {
            return next(err);
        }
        bcrypt.hash(user.password, salt, null, (err, hash) => {
            if (err) {
                return next(err);
            }
            user.password = hash;
            next();
        });
    });
});

/**
 * Helper method for validating user's password.
 */
userSchema.methods.comparePassword = function (candidatePassword, cb) {
    bcrypt.compare(candidatePassword, this.password, (err, isMatch) => {
        //cb(err, isMatch);
        if (err) {
            return cb(err);
        }
        cb(null, isMatch);
    });
};


const User = mongoose.model('User', userSchema);

module.exports = User;