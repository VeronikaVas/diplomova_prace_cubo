/*eslint-disable */
var Canvas = {}

/* for displaying painting */
Canvas.ctx = null
Canvas.c = null
Canvas.color = '#fff'
Canvas.windowCanvas = null
Canvas.canSet = false
Canvas.actualPosition = {
    x: 0,
    y: 0
}

Canvas.showPosition = true

Canvas.getCanvas = function () {
    return Canvas.c
}

Canvas.initContext = function () {
    Canvas.c = new OffscreenCanvas(256, 256)
    Canvas.ctx = Canvas.c.getContext('2d')
    Canvas.ctx.imageSmoothingEnabled = true
    Canvas.ctx.lineCap = 'round'
    Canvas.ctx.canvas.height = 400
    Canvas.ctx.canvas.width = 400
    Canvas.actualPosition = {
        x: 200,
        y: 200
    }

    //Canvas.currentPosition("#000")
}

Canvas.drawPositionAutomatically = function () {
    if (Canvas.showPosition) {
        Canvas.currentPosition("#000");
    }
}

Canvas.setBackgroundColor = function (c) {
    if (Canvas.canSet) {
        Canvas.ctx.canvas.style.backgroundColor = c;
    }
    Canvas.color = c;
}

Canvas.setSize = function (val, option) {
    if (option == "WIDTH") {
        Canvas.ctx.canvas.width = val;
    }

    if (option == "HEIGHT") {
        Canvas.ctx.canvas.height = Number(val);
    }

    Canvas.actualPosition = {
        x: Canvas.ctx.canvas.width / 2,
        y: Canvas.ctx.canvas.height / 2
    }
}

Canvas.getSize = function (option) {
    var result = 400;

    if (option == "WIDTH") {
        result = Canvas.ctx.canvas.width;
    }

    if (option == "HEIGHT") {
        result = Canvas.ctx.canvas.height;
    }
    return result;
}

Canvas.move = function (x, y) {
    Canvas.actualPosition.x = x;
    Canvas.actualPosition.y = y;
    Canvas.ctx.moveTo(x, y);
}

Canvas.moveDistance = function (distance, angle) {
    var x = Canvas.actualPosition.x
    var y = Canvas.actualPosition.y
    var i = distance * Math.cos(Canvas.degreesToRadians(-angle)) + x
    var j = distance * Math.sin(Canvas.degreesToRadians(-angle)) + y
    Canvas.ctx.moveTo(i, j)
    Canvas.actualPosition.x = i
    Canvas.actualPosition.y = j
}

Canvas.moveDistance2 = function (distance, angle) {
    var x = Canvas.actualPosition.x
    var y = Canvas.actualPosition.y
    var i = distance * Math.cos(-angle) + x
    var j = distance * Math.sin(-angle) + y
    Canvas.ctx.moveTo(i, j)
    Canvas.actualPosition.x = i
    Canvas.actualPosition.y = j
}

Canvas.moveSelect = function (option) {
    var i, j
    switch (option.valueOf()) {
        case 'CENTER':
            i = Canvas.ctx.canvas.width / 2
            j = Canvas.ctx.canvas.height / 2
            break
        case 'TLEFT':
            i = 0
            j = 0
            break
        case 'TRIGHT':
            i = Canvas.ctx.canvas.width
            j = 0
            break
        case 'DLEFT':
            i = 0
            j = Canvas.ctx.canvas.height
            break
        case 'DRIGHT':
            i = Canvas.ctx.canvas.width
            j = Canvas.ctx.canvas.height
            break
    }
    Canvas.ctx.moveTo(i, j)
    Canvas.actualPosition.x = i
    Canvas.actualPosition.y = j

}

Canvas.currentPosition = function (c) {
    var origin = Canvas.ctx.fillStyle;
    Canvas.ctx.strokeStyle = c;
    Canvas.ctx.beginPath();

    var x = Canvas.actualPosition.x;
    var y = Canvas.actualPosition.y;
    Canvas.ctx.moveTo(x - 5, y);
    Canvas.ctx.lineTo(x + 5, y);
    Canvas.ctx.stroke();

    Canvas.ctx.moveTo(x, y - 5);
    Canvas.ctx.lineTo(x, y + 5);
    Canvas.ctx.stroke();

    Canvas.ctx.strokeStyle = origin;
}

Canvas.actualCoordinate = function (o) {
    if (o == "X") {
        return Canvas.actualPosition.x;
    }

    if (o == "Y") {
        return Canvas.actualPosition.y;
    }
}

/**cary a barvy */
Canvas.setFillColor = function (color) {
    Canvas.ctx.fillStyle = color
}

Canvas.setStrokeColor = function (color) {
    Canvas.ctx.strokeStyle = color
}

Canvas.setStrokeWidth = function (n) {
    Canvas.ctx.lineWidth = n
}

Canvas.setStrokeCap = function (n) {
    if (n.valueOf() === 'ROUND') {
        Canvas.ctx.lineCap = 'round'
    } else {
        Canvas.ctx.lineCap = 'butt'
    }
}

Canvas.drawLine = function (distance, angle, stroke) {
    if (stroke.data) {
        Canvas.ctx.beginPath()
    }
    var x = Canvas.actualPosition.x
    var y = Canvas.actualPosition.y

    if (stroke.data) {
        Canvas.ctx.moveTo(x, y)
    }

    var i = distance * Math.cos(Canvas.degreesToRadians(-angle)) + x
    var j = distance * Math.sin(Canvas.degreesToRadians(-angle)) + y

    Canvas.ctx.lineTo(Math.round(i), Math.round(j))
    if (stroke.data) {
        Canvas.ctx.stroke()
    }
    Canvas.actualPosition.x = i
    Canvas.actualPosition.y = j
}

Canvas.drawRect = function (fill, width, height) {
    var x = Canvas.actualPosition.x
    var y = Canvas.actualPosition.y
    console.log(fill)
    if (fill == "FILLED") {
        Canvas.ctx.fillRect(x, y, width, height)
    }

    if (fill == "STROKE") {
        //Canvas.ctx.globalCompositeOperation = 'source-over'

        Canvas.ctx.strokeRect(x, y, width, height)
    }
}

Canvas.drawCircle = function (fill, r) {
    var x = Canvas.actualPosition.x
    var y = Canvas.actualPosition.y
    if (fill == "FILLED") {
        Canvas.ctx.beginPath()
        Canvas.ctx.arc(x, y, r, 0, 2 * Math.PI)
        Canvas.ctx.fill()
    } if (fill == "STROKE") {
        Canvas.ctx.beginPath()
        Canvas.ctx.arc(x, y, r, 0, 2 * Math.PI)
        Canvas.ctx.stroke()
    }
}

Canvas.drawArc = function (fill, r, from, to) {
    var x = Canvas.actualPosition.x
    var y = Canvas.actualPosition.y
    if (fill == "FILLED") {
        Canvas.ctx.beginPath()
        Canvas.ctx.arc(x, y, r, Canvas.degreesToRadians(from), Canvas.degreesToRadians(to))
        Canvas.ctx.fill()
    } if (fill == "STROKE") {
        Canvas.ctx.beginPath()
        Canvas.ctx.arc(x, y, r, Canvas.degreesToRadians(from), Canvas.degreesToRadians(to))
        Canvas.ctx.stroke()
    }
}

Canvas.openPath = function () {
    Canvas.ctx.beginPath()
    Canvas.ctx.moveTo(Canvas.actualPosition.x, Canvas.actualPosition.y)
}

Canvas.closePath = function (fill) {
    Canvas.ctx.closePath()
    if (fill == "FILLED") {
        Canvas.ctx.fill()
    } else {
        Canvas.ctx.stroke()
    }
}

Canvas.setFont = function (font, size) {
    var f = 'Arial'
    switch (font.valueOf()) {
        case 'ARIAL':
            f = 'Arial'
            break
        case 'TIMES':
            f = 'Times'
            break

        case 'COURIER':
            f = 'Courier'
            break
    }

    Canvas.ctx.font = size + 'px ' + f
}

Canvas.drawText = function (fill, text) {
    if (fill == "FILLED") {
        Canvas.ctx.fillText(text, Canvas.actualPosition.x, Canvas.actualPosition.y)
    }

    else {
        Canvas.ctx.strokeText(text, Canvas.actualPosition.x, Canvas.actualPosition.y)
    }
}


Canvas.degreesToRadians = function (degrees) {
    var pi = Math.PI
    return degrees * (pi / 180)
}

Canvas.initInterpreter = function (interpreter, scope) {
    var wrapper;

    wrapper = function (c) {
        Canvas.setBackgroundColor(c)
    }
    interpreter.setProperty(scope, 'setBackgroundColorCubo',
        interpreter.createNativeFunction(wrapper))

    wrapper = function (val, option) {
        Canvas.setSize(val, option)
    }
    interpreter.setProperty(scope, 'setSizeCubo',
        interpreter.createNativeFunction(wrapper))

    wrapper = function (o) {
        return interpreter.createPrimitive(Canvas.getSize(o));
    }
    interpreter.setProperty(scope, 'getSizeCubo',
        interpreter.createNativeFunction(wrapper))

    wrapper = function (x, y) {
        Canvas.move(x, y)
    }
    interpreter.setProperty(scope, 'moveXYCubo',
        interpreter.createNativeFunction(wrapper))

    wrapper = function (x, y) {
        Canvas.moveDistance(x, y)
    }
    interpreter.setProperty(scope, 'moveDistanceCubo',
        interpreter.createNativeFunction(wrapper))

    wrapper = function (x, y) {
        Canvas.moveDistance(x, y)
    }
    interpreter.setProperty(scope, 'moveDistance2Cubo',
        interpreter.createNativeFunction(wrapper))

    wrapper = function (x) {
        Canvas.moveSelect(x)
    }
    interpreter.setProperty(scope, 'moveSelectCubo',
        interpreter.createNativeFunction(wrapper))

    wrapper = function (x) {
        Canvas.currentPosition(x)
    }
    interpreter.setProperty(scope, 'pointPositionCubo',
        interpreter.createNativeFunction(wrapper))

    wrapper = function (o) {
        return interpreter.createPrimitive(Canvas.actualCoordinate(o));
    }
    interpreter.setProperty(scope, 'getCoordinateCubo',
        interpreter.createNativeFunction(wrapper))

    wrapper = function (color) {
        Canvas.setFillColor(color)
    }
    interpreter.setProperty(scope, 'setFillColorCubo',
        interpreter.createNativeFunction(wrapper))

    wrapper = function (color) {
        Canvas.setStrokeColor(color)
    }
    interpreter.setProperty(scope, 'setStrokeColorCubo',
        interpreter.createNativeFunction(wrapper))

    wrapper = function (color) {
        Canvas.setStrokeWidth(color)
    }
    interpreter.setProperty(scope, 'setStrokeWidthCubo',
        interpreter.createNativeFunction(wrapper))
    wrapper = function (c) {
        Canvas.setStrokeCap(c)
    }
    interpreter.setProperty(scope, 'setStrokeCapCubo',
        interpreter.createNativeFunction(wrapper))

    wrapper = function (distance, angle, stroke) {
        Canvas.drawLine(distance, angle, stroke)
    }
    interpreter.setProperty(scope, 'drawLineCubo',
        interpreter.createNativeFunction(wrapper))

    wrapper = function (fill, w, h) {
        Canvas.drawRect(fill, w, h)
    }
    interpreter.setProperty(scope, 'drawRectCubo',
        interpreter.createNativeFunction(wrapper))

    wrapper = function (fill, r) {
        Canvas.drawCircle(fill, r)
    }
    interpreter.setProperty(scope, 'drawCircleCubo',
        interpreter.createNativeFunction(wrapper))

    wrapper = function (fill, r, from, to) {
        Canvas.drawArc(fill, r, from, to)
    }
    interpreter.setProperty(scope, 'drawArcCubo',
        interpreter.createNativeFunction(wrapper))

    wrapper = function (fill) {
        Canvas.closePath(fill)
    }
    interpreter.setProperty(scope, 'closePathCubo',
        interpreter.createNativeFunction(wrapper))

    wrapper = function () {
        Canvas.openPath()
    }
    interpreter.setProperty(scope, 'openPathCubo',
        interpreter.createNativeFunction(wrapper))

    wrapper = function (font, size) {
        Canvas.setFont(font, size)
    }
    interpreter.setProperty(scope, 'setFontCubo',
        interpreter.createNativeFunction(wrapper))

    wrapper = function (fill, text) {
        Canvas.drawText(fill, text)
    }
    interpreter.setProperty(scope, 'drawTextCubo',
        interpreter.createNativeFunction(wrapper))
}

/*eslint-enable */