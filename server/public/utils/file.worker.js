onmessage = e => {
  var reader = new FileReader()
  reader.addEventListener('loadend', function () {
    postMessage(reader.result)
  })
  reader.readAsText(e.data, 'UTF-8')
}
