const AuthenticationController = require("./controllers/AuthenticationController");
const UserController = require("./controllers/UserController");
const TaskController = require("./controllers/TaskController");
const EmailController = require("./controllers/EmailController");
const SolutionController = require("./controllers/SolutionController");
const isAuthenticated = require("./controllers/isAuthenticated");
module.exports = app => {
  app.post("/api/register", AuthenticationController.register);
  app.post("/api/login", AuthenticationController.login);
  app.post('/api/send', EmailController.sendEmail);


  /** User routes */
  app.get("/api/user/info", isAuthenticated, UserController.statistics);

  app.delete("/api/user", isAuthenticated, UserController.deactivate);
  app.put("/api/user/password", isAuthenticated, UserController.change);


  /** Task routes */
  app.get("/api/tasks", isAuthenticated, TaskController.getAll)
  app.get("/api/tasks/:id", isAuthenticated, TaskController.task)
  app.delete("/api/tasks/:id", isAuthenticated, TaskController.remove)
  app.post("/api/tasks", isAuthenticated, TaskController.createTask);
  app.put("/api/tasks", isAuthenticated, TaskController.editTask);

  app.post("/api/solution", isAuthenticated, SolutionController.create);

};